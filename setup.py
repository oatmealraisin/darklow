from setuptools import setup


def readme():
    """
    Function to read the long description for the Darklow package.
    """
    with open('README.md') as _file:
        return _file.read()


setup(
    name='darklow',
    version='1.0.0',
    description="Library for launching a Darklow bot",
    long_description=readme(),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/aberoth-gdc/darklow',
    author='Twoshields',
    classifiers=[
        "Topic :: Software Development :: Libraries :: Python Modules"
    ],
    packages=['darklow'],
    install_requires=['discord'],
    python_requires='>=3.9',
    zip_safe=False)
