"""
"""

import logging
import os

import discord
from discord.errors import Forbidden

import darklow


logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

TOKEN: str = os.getenv('DISC_BOT_TOKEN', "")
logger.info("Discord token: %s" % TOKEN)

client = discord.Client()


@client.event
async def on_ready():
    """
    TODO
    """
    logger.info("===== DARKLOW BEGIN =====")

    logger.info("DARKLOW IS READY")
    logger.info("DARKLOW USER ID: {}".format(client.user.id))

    return


@client.event
async def on_message(message):
    """
    TODO
    """
    logger.info("{}, #{}, {}: {!r}".format(message.guild, message.channel, message.author, message.content))

    # Don't bother with Tavelor's messages
    if message.author == client.user:
        return

    if len(message.content) == 0:
        return

    # handle when we're not mentioned
    if darklow.get_discord_id(message.content.split()[0]) != client.user.id:
        return

    cmd = darklow.parse_command(message)
    msg: str = await cmd(message)

    if msg != "":
        logger.debug(msg)
        # Try to do a fancy reply, otherwise just send to channel.
        try:
            await message.reply(msg)
        except Forbidden:
            await message.channel.send(msg)


client.run(TOKEN)
