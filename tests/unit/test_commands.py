import darklow.bot as b

class TestRegex():

    def test_trivia(self):
        pass

    def test_no_match(self):
        pass

    def test_hello(self):
        pass

    def test_simple_flip(self):
        assert b.re_simple_flip.match("flip a b 2") is not None
