import darklow.games as g

class TestHorT():

    def test_normal_create(self):
        participants = ["foo", "bar"]
        game = g.HorT(participants)
        assert game.name == "H or T"
        assert len(game.participants) == 2
        assert game.participants[0] == participants[0]
        assert game.participants[1] == participants[1]
