"""
Utility functions for the Darklow bot
"""

from collections import deque
from datetime import datetime
from itertools import chain
import logging
import re


discord_id = re.compile(r'^(?P<o><)?(@!?)?(?P<id>\d{15,20})(?(o)>|)$')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def is_discord(ident):
    """
    Checks if the input is a valid Discord mention/id
    """
    return (
        (
            isinstance(ident, int)
            # TODO: Get a better test
            and ident >= 100000000000000
        ) or (
            isinstance(ident, str) and discord_id.match(ident) is not None
        )
    )


def get_discord_id(ident):
    """
    Returns the integer value Discord identity
    """
    logger.debug("Getting ID from {}".format(ident))

    if isinstance(ident, int) and 1000000000000000000 > ident >= 100000000000000000:
        logger.debug("Passed an int, returning it as is")
        return ident
    if not is_discord(ident):
        logger.debug("Not a discord ID: {}".format(ident))
        return 0
    logger.debug("Returning {}".format(discord_id.match(ident).groupdict()["id"]))

    return int(discord_id.match(ident).groupdict()["id"])


def preprocess_input(content: str) -> list[str]:
    """
    Runs a client input through a series of checks and changes.
    """
    output = []
    changed = False

    if isinstance(content, str):
        content = content.split()

    for i, word in enumerate(content):
        if isinstance(word, (float,int)):
            output.append(word)
            continue
        word = word.lower().replace('?', '')
        if is_discord(word):
            output.append(get_discord_id(word))
        elif word == "":
            continue
        else:
            output.append(word)

    return output
