"""
Functions relating to the bot.
"""

import logging
import json
import random
import re
import typing

import discord

from .flips import standard_flipset
from .util import preprocess_input


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

trivia: dict[str, str] = {
    'hello': 'Wecome, {}. How can I help?',
}

trivia['hi'] = trivia['hello']
trivia['yo'] = trivia['hello']
trivia['wassup'] = trivia['hello']
trivia['wazzup'] = trivia['hello']

re_simple_flip = re.compile("|".join([
    'flip (\w+ )+\d',
]))

BETA = 0
HIGH = 1
NORMAL = 2
LOW = 3
NO_PERM = 1000

def _authorized(level: int, message: discord.Message) -> bool:
    logger.debug("Checking auth level for {}".format(message.guild.id))
    auth_level: dict[int,int] = {
        870383494327570512: BETA, # GDC
        825412898372321290: NORMAL, # Banabread
        878722012351180801: BETA, # Red/Blue
        689863485554819129: LOW, # Legion PVP
        370780258141601792: NO_PERM, # Glowbot
    }

    if message.guild.id not in auth_level:
        # We don't know about this guild
        return False

    return auth_level[message.guild.id] <= level


def parse_command(message: discord.Message) -> typing.Callable[[typing.Any], str]:
    content: list[str] = preprocess_input(message.content)
    if len(content) ==  1:
        return greeting
    if len(content) == 2 and content[1].lower() in trivia:
        return get_trivia
    if re_simple_flip.match(" ".join(content[1:]).lower()) is not None:
        return simple_flip

    return no_match


def not_authorized(message: discord.Message) -> str:
    """
    Give a message if the user tries to access a command from a Guild without
    sufficient permissions.
    """
    not_authorized_message: str = "Greetings, {}. I can't tell you about that."
    return not_authorized_message.format(message.author.mention)


async def no_match(message: discord.Message) -> str:
    """
    Provided when we don't know what they're talking about.
    """
    no_match_message: str = "Welcome, {}. That does not interest me."
    return no_match_message.format(message.author.mention)


async def greeting(message: discord.Message) -> str:
    """
    They only said our name.
    """
    return trivia['hi'].format(message.author.mention)


async def get_trivia(message: discord.Message) -> str:
    """
    Return some trivia that you could get from talking to the NPC in game.
    Generally only one-word phrases
    """
    content: list[str] = preprocess_input(message.content)
    return trivia[content[1].lower()].format(message.author.mention)


async def simple_flip(message: discord.Message) -> str:
    content: list[str] = preprocess_input(message.content)
    logger.debug(content)

    mocks: list[str] = [
        "Hah! Obviously {} carried you all.\n",
        "I'll do it, but you should know, {}, that {} didn't really contribute.\n",
        "Who did you steal this from? No matter, let's begin.\n",
    ]


    msg: str = mocks[1].format(message.author.mention, random.sample(content[2:-1],1)[0])
    logger.debug(msg)

    count: int = int(content[-1])
    participants = content[2:-1]
    logger.debug(participants)
    games, placings = standard_flipset(participants, count)

    h_emoji: str = ":regional_indicator_h:"
    h_bold: str = "**H**"
    t_emoji: str = ":regional_indicator_t:"
    t_bold: str = "**T**"

    for i, game in enumerate(games):
        post: str = f" between {', '.join(game.participants)}"
        msg += f"__Round {i+1}: {game.name}__{post}\n"
        for result in game.results:
            msg += "> "
            msg += ", ".join(
                [
                    "{}: {}".format(
                        x[0].capitalize(), h_bold if x[1] else t_bold
                    ) for x in result.items()
                ]
            )
            msg += "\n"
        if len(game.winners) == 1 and isinstance(game.winners, list):
            msg += f"> **Winner**: {game.winners[0]}\n"
        else:
            msg += f"> **Winners**: {', '.join(game.winners)}\n"

    # https://stackoverflow.com/questions/9647202/ordinal-numbers-replacement
    ordinal = lambda n: "%d%s" % (n,"tsnrhtdd"[(n//10%10!=1)*(n%10<4)*n%10::4])
    msg += "\n".join(
        [f"{ordinal(i+1)}: {', '.join(p)}" for i,p in enumerate(placings)]
    )

    return msg
