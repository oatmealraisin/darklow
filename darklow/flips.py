import json
import logging
import random
import typing

from .games import Game, HorT, OddsWins, HeadsIn, Uncontested

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def standard_flipset(
    participants: list[str],
    num_prizes: int
) -> tuple[list[Game], list[list[str]]]:
    logger.info(
        f"Flipping for {num_prizes} prizes between {', '.join(participants)}."
    )

    # If we have less places than participants, just flip for each place.
    # If we have more places than participants, flip for pick order.
    num_places = min(num_prizes, len(participants))
    places: list[list[str]] = [participants for _ in range(num_places)]
    games: list[Game] = []
    r: int = 0

    for i in range(len(places)):
        game: Game

        while len(places[i]) > 1:
            if len(places[i]) == 1:
                game = Uncontested(places[i][:])
            elif len(places[i]) == 2:
                game = HorT(places[i][:])
            elif len(places[i]) == 3:
                game = OddsWins(places[i][:])
            elif len(places[i]) >= 4:
                game = HeadsIn(places[i][:])
            else:
                logger.error(len(places[i]))
                logger.error(json.dumps(places[i]))
                raise NotImplementedError

            winners: list[str] = game.play()
            games.append(game)

            for j in range(len(places[i:])):
                if j >= len(winners):
                    places[j+i] = [x for x in places[j+i] if x not in winners]
                else:
                    places[j+i] = winners[:]

            places[i] = winners

            r+=1
            logger.info(
                f"Round {r} ({game.name}): {json.dumps(game.results)}\n" +
                f"\tWinner: {json.dumps(game.winners)}"
            )
            logger.debug(json.dumps(places,indent=2))

    return games, places
