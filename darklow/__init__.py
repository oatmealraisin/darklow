
import logging

from .util import preprocess_input
from .util import get_discord_id
from .bot import parse_command


logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)
