import json
import logging
import random
from typing import Iterator, Generator

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Game():
    """
    Class representing a Game, its results, and its winners. Games are normally
    initialized unplayed, and must be played before a winner is decided.
    """

    def __init__(self, name: str, participants: list[str]):
        self.name: str = name
        self.participants: list[str] = participants
        self.winners: list[str] = []
        self.results: list[dict[str, bool]] = []


    def flip(self) -> bool:
        """
        Flips a fair coin and returns it. True for heads, False for tails.
        """
        return random.random() >= 0.5


    def next(self) -> dict[str,bool]:
        """
        Plays one round of the game, returning a result.
        """
        result: dict[str,bool] = {}

        for participant in self.participants:
            result[participant] = self.flip()

        logger.debug(f"{self.name}: {json.dumps(result)}")

        return result


    def play(self) -> list[str]:
        """
        Plays the game until a winner or multiple winners are found.
        """
        raise NotImplementedError


    def replay(self) -> list[str]:
        """
        Replays the game from scratch. Resets the results.
        """
        self.reset()
        return self.play()


    def reset(self) -> None:
        """
        Resets the game to an unplayed state.
        """
        self.winners = []
        self.results = []


class Uncontested(Game):
    """
    An uncontested game. All participants are 'winners'
    """

    def __init__(self, participants: list[str]):
        super().__init__("Uncontested", participants)
        self.winners = participants


    def play(self) -> list[str]:
        return self.winners


class MatchHouse(Game):
    """
    A game between two players. Each player flips a coin. The house flips a
    third coin. The play who's coin face matches the house wins. If both or
    neither player matches, the result is a draw and the game must be replayed.
    """

    def __init__(self, participants):
        assert len(participants) == 2
        super().__init__("Match House", participants)


    def play(self) -> list[str]:
        """
        Plays the game until a winner is found. Returns the winner
        """
        while len(self.winners) == 0:
            result: dict[str,bool] = self.next()
            house: bool = self.flip()
            # Check that not all coins match
            if not all(result.values()) and any(result.values()):
                if result[self.participants[0]] == house:
                    # Both players cannot win.
                    assert len(self.winners) == 0
                    self.winners = [self.participants[0]]
                if result[self.participants[1]] == house:
                    # Both players cannot win.
                    assert len(self.winners) == 0
                    self.winners = [self.participants[1]]

            result["house"] = house
            self.results.append(result)

        return self.winners


class OddsWins(Game):

    def __init__(self, participants: list[str]):
        assert len(participants) == 3
        super().__init__("Odds Wins", participants)


    def play(self) -> list[str]:
        while len(self.winners) == 0:
            result: dict[str,bool] = self.next()
            self.results.append(result)
            # Check that not all coins match
            if not all(result.values()) and any(result.values()):
                if len([x[0] for x in result.items() if x[1]]) == 1:
                    self.winners = [x[0] for x in result.items() if x[1]]
                elif len([x[0] for x in result.items() if not x[1]]) == 1:
                    self.winners = [x[0] for x in result.items() if not x[1]]


        return self.winners


class HeadsIn(Game):
    def __init__(self, participants: list[str]):
        super().__init__("Heads In", participants)


    def play(self) -> list[str]:
        while len(self.winners) == 0:
            result: dict[str,bool] = self.next()
            self.results.append(result)

            if not all(result.values()) and any(result.values()):
                self.winners = [x[0] for x in result.items() if x[1]]

        return self.winners


class HorT(Game):
    def __init__(self, participants: list[str]):
        super().__init__("H or T", participants)

    def play(self) -> list[str]:
        assert len(self.participants) == 2

        call_player: str = random.choice([0,1])
        call: bool = self.flip()
        response: bool = self.flip()

        self.results.append({
            f"{self.participants[call_player]} calls": call,
            self.participants[call_player-1]: response,
        })

        if call == response:
            self.winners = [self.participants[call_player]]
        else:
            self.winners = [self.participants[call_player-1]]

        return self.winners
